using Gtk 4.0;
using Adw 1;

menu generalMenu {
    section {
        item (_('Save'), 'app.save')
        item (_('Save as...'), 'app.saveas')
    }
    section {
        item (_('Preferences'), 'app.preferences')
        item (_('Keyboard Shortcuts'), 'app.shortcuts')
        item (_('About MD Writer'), 'app.about')
    }
}

menu previewModeMenu {
    section {
        label: _('Preview Mode');
        item {
            label: _('Vertical');
            action: 'app.preview_mode';
            target: 'vertical';
        }
        item {
            label: _('Horizontal');
            action: 'app.preview_mode';
            target: 'horizontal';
        }
        item {
            label: _('Disabled');
            action: 'app.preview_mode';
            target: 'disabled';
        }
    }
}

template AppContent : Box {
    orientation: vertical;
    hexpand: true; vexpand: true;
    WindowHandle {
        vexpand: false;
        HeaderBar headerbar {
            show-title-buttons: true;
            [title] Box {
                orientation: horizontal;
                Image unsaved_icon {
                    icon-name: 'unsaved-symbolic';
                    tooltip-text: _('Unsaved Changes');
                    visible: false;
                }
                Label title_label {
                    styles ['title']
                    label: 'MD Writer';
                    ellipsize: end;
                    wrap: false;
                }
            }
            Button new_btn {
                icon-name: 'tab-new-symbolic';
                tooltip-text: _('New Document');
                clicked => on_new_clicked();
            }
            Adw.SplitButton open_splitbtn {
                label: _('Open');
                popover: Popover recent_files_popover {
                    height-request: 280; width-request: 250;
                    ScrolledWindow {
                        hexpand: true; vexpand: true;
                        hscrollbar-policy: never;
                        ListBox recent_files_listbox {
                            styles ['navigation-sidebar']
                            selection-mode: none;
                            row-activated => on_recent_files_listbox_row_activated();
                        }
                    }
                };
                clicked => on_open_clicked();
            }
            [end] MenuButton menu_btn {
                tooltip-text: _('Menu');
                icon-name: 'open-menu-symbolic';
                menu-model: generalMenu;
            }
            [end] MenuButton preview_mode_menu_btn {
                tooltip-text: _('Preview Mode');
                icon-name: 'view-reveal-symbolic';
                menu-model: previewModeMenu;
            }
        }
    }
    Stack stack {
        visible-child: empty_state;
        StackPage {
            child: Adw.StatusPage empty_state {
                icon-name: 'emblem-documents-symbolic';
                title: _('No Files Open');
                Box {
                    orientation: vertical;
                    spacing: 12;
                    Button {
                        styles ['pill']
                        halign: center;
                        label: _('New Document');
                        action-name: 'app.new';
                    }
                    Button {
                        styles ['pill']
                        halign: center;
                        label: _('Open...');
                        action-name: 'app.open';
                    }
                }
            };
        }
        StackPage {
            child: Box main_content {
                orientation: vertical;
                Adw.TabBar tabbar {
                    vexpand: false; hexpand: true;
                    view: tabview;
                }
                Adw.TabView tabview {
                    hexpand: true; vexpand: true;
                    close-page => on_tabview_close_page();
                    notify::selected-page => on_page_selected();
                    notify::n-pages => on_num_pages_changed();
                }
            };
        }
    }
}
