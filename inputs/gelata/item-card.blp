using Gtk 4.0;
using Adw 1;

template ItemCard : Box {
  Button link {
    use-underline: true;
    valign: start;

    styles [
      "card",
    ]

    Box container {
      valign: start;
      orientation: vertical;
      spacing: 3;
      margin-top: 12;
      margin-bottom: 12;
      margin-start: 12;
      margin-end: 12;

      Overlay {
        [overlay]
        LevelBar progress {
          focusable: false;
          valign: end;
          halign: fill;
          visible: false;
          max-value: 100;
          margin-top: 9;
          margin-bottom: 9;
          margin-start: 9;
          margin-end: 9;
        }

        [overlay]
        Image played {
          icon-name: "selection-mode-symbolic";
          pixel-size: 20;
          focusable: false;
          valign: start;
          halign: end;
          visible: false;
          margin-top: 9;
          margin-end: 9;

          styles [
            "accent",
          ]
        }

        Box {
          Image placeholder {
            focusable: false;
            vexpand: true;
            valign: start;
            halign: center;
            overflow: hidden;
            icon-name: "twitter-profile-symbolic";
            icon-size: large;

            styles [
              "card",
            ]
          }

          Adw.Avatar avatar {
            focusable: false;
            vexpand: true;
            valign: start;
            halign: center;
            overflow: hidden;
            visible: false;
            size: 150;
            show-initials: true;
          }

          Picture image {
            focusable: false;
            vexpand: true;
            valign: start;
            halign: center;
            overflow: hidden;
            visible: false;

            styles [
              "card",
            ]
          }
        }
      }

      Label title {
        focusable: false;
        justify: center;
        ellipsize: end;
        max-width-chars: 0;
        lines: 1;
        visible: false;
        margin-top: 3;

        styles [
          "heading",
        ]
      }

      Label subtitle {
        focusable: false;
        visible: false;
        ellipsize: end;
        max-width-chars: 0;
        lines: 1;

        styles [
          "caption",
        ]
      }
    }
  }
}
