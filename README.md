# Blueprint regression tests

This repository contains a bunch of blueprint files found in the wild and their
expected outputs. The [main blueprint-compiler repo](https://gitlab.gnome.org/jwestman/blueprint-compiler)
uses these tests in its CI to make sure stuff doesn't break.

Whenever this repository is updated, the corresponding commit hash in
blueprint-compiler's .gitlab-ci.yml needs to be updated.

# Licenses

adwsteamgtk:
<https://github.com/Foldex/AdwSteamGtk>
GPLv3

appaya:
<https://git.sr.ht/~fabrixxm/appaya>
GPLv3

ariane:
<https://github.com/ArianeTeam/Ariane>
GPLv3

bluetype:
<https://github.com/mjakeman/bluetype>
GPLv3

blurble:
<https://gitlab.gnome.org/World/Blurble>
GPLv3

bottles:
<https://github.com/bottlesdevs/Bottles>
GPLv3

cartridges:
<https://github.com/kra-mo/cartridges>
GPLv3

commit:
<https://github.com/sonnyp/Commit>
GPLv3

confy:
<https://sr.ht/~fabrixxm/Confy/>
GPLv3

converter:
<https://gitlab.com/adhami3310/Converter>
GPLv3

damask:
<https://gitlab.gnome.org/subpop/damask>
GPLv3

denaro:
<https://github.com/nlogozzo/NickvisionMoney>
MIT

dialect:
<https://github.com/dialect-app/dialect/>
GPLv3

dynamic-wallpaper:
<https://github.com/dusansimic/dynamic-wallpaper>
GPLv2

extension-manager:
<https://github.com/mjakeman/extension-manager>
GPLv3

favagtk:
<https://gitlab.gnome.org/johannesjh/favagtk>
GPLv3

file-shredder:
<https://github.com/ADBeveridge/raider>
GPLv3

gdm-settings:
<https://github.com/realmazharhussain/gdm-settings>
AGPLv3

gelata:
<https://gitlab.com/bscubed/gelata>
GPLv3

geopard:
<https://github.com/ranfdev/Geopard>
GPLv3

gfeeds:
<https://gitlab.gnome.org/World/gfeeds>
GPLv3

giara:
<https://gitlab.gnome.org/World/giara>
GPLv3

girens:
<https://gitlab.gnome.org/tijder/girens>
GPLv3

gradience:
<https://github.com/GradienceTeam/Gradience>
GPLv3

health:
<https://gitlab.gnome.org/World/Health>
GPLv3

hydrapaper:
<https://gitlab.gnome.org/GabMus/HydraPaper>
GPLv3

identity:
<https://gitlab.gnome.org/YaLTeR/identity>
GPLv3

junction:
<https://github.com/sonnyp/Junction>
GPLv3

logistics:
<https://github.com/cameronthecoder/logistics>
GPLv3

maniatic-launcher:
<https://github.com/santiagocezar/maniatic-launcher>
GPLv3

mdwriter:
<https://gitlab.gnome.org/GabMus/mdwriter>
GPLv3

millionaire:
<https://github.com/martinszeltins/who-wants-to-be-a-millionaire>
GPLv2

newcaw:
<https://github.com/CodedOre/NewCaw>
GPLv3

paper:
<https://gitlab.com/posidon_software/paper>
GPLv3

passes:
<https://github.com/pablo-s/passes>
GPLv3

playhouse:
<https://github.com/sonnyp/Playhouse>
GPLv3

plitki:
<https://github.com/YaLTeR/plitki>
GPLv3

raider:
<https://github.com/ADBeveridge/raider>
GPLv3

retro:
<https://github.com/sonnyp/Retro>
GPLv3

solanum:
<https://gitlab.gnome.org/World/Solanum/>
GPLv3

swatch:
<https://gitlab.gnome.org/GabMus/swatch>
GPLv3

tangram:
<https://github.com/sonnyp/Tangram>
GPLv3

textpieces:
<https://github.com/liferooter/textpieces>
GPLv3

tube-converter:
<https://github.com/nlogozzo/NickvisionTubeConverter>
MIT

upscaler:
<https://gitlab.com/TheEvilSkeleton/Upscaler>
GPLv3

video-trimmer:
<https://gitlab.gnome.org/YaLTeR/video-trimmer>
GPLv3 (based on Cargo.toml)

wayfarer:
<https://github.com/stronnag/wayfarer>
GPLv3

whatip:
<https://gitlab.gnome.org/GabMus/whatip>
GPLv3

workbench:
<https://github.com/sonnyp/Workbench>
GPLv3, some files are CC0 1.0

