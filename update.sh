#!/usr/bin/env bash

blueprint-compiler batch-compile expected inputs $(find inputs -name *.blp)
